package com.example.adarsh.nexteduv01;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profilepage extends AppCompatActivity {

    TextView textview_name,textview_email,textview_contact,textview_pass,textview_preference,
            textview_locpref,textview_exp,textview_subject,textview_desc,textview_travel,mydetails;
    Button logout,edit_profile,view_course;
    CircleImageView profileimage;
    AlertDialog alertdialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilepage);
        logout=findViewById(R.id.logout);
        view_course=findViewById(R.id.view_course);
        edit_profile=findViewById(R.id.edit_profile);
        profileimage=findViewById(R.id.profileimage);
        textview_name=findViewById(R.id.textview_name);
        textview_email=findViewById(R.id.textview_email);
        textview_contact=findViewById(R.id.textview_contact);
        textview_pass =findViewById(R.id.textview_pass);
        textview_preference=findViewById(R.id.textview_preference);
        textview_locpref =findViewById(R.id.textview_locpref);
        textview_exp=findViewById(R.id.textview_exp);
        textview_subject=findViewById(R.id.textview_subject);
        textview_desc=findViewById(R.id. textview_desc);
        textview_travel=findViewById(R.id.textview_travel);
        mydetails= findViewById(R.id.mydetails);

        mydetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(getApplicationContext(),Coachpersonaldetails.class);
                startActivity(intent);
            }
        });


        view_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Course_list.class);
                startActivity(intent);

            }
        });

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Editprofile.class);
                startActivity(intent);
            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog = new AlertDialog.Builder(Profilepage.this).create();

                alertdialog.setTitle("Logout");
                alertdialog.setMessage("Are you sure ! logout ?");
                alertdialog.setCancelable(false);
                alertdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alertdialog.dismiss();

                    }
                });

                alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.clear();
                        editor.commit();
                        Intent intent = new Intent(Profilepage.this, MainActivity.class);
                        startActivity(intent);
                        //if the user is logged out then change the following
                        SharedPreferences setting = getSharedPreferences("Checkstatus", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = setting.edit();
                        editor1.putBoolean("hasLoggedIn",false);
                        editor1.apply();
                        Profilepage.this.finish();
                    }
                });
                alertdialog.show();
            }
        });

        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String name = shared.getString("name","");
        String email =shared.getString("email","");
        String contact = shared.getString("contact","");
        String pass = shared.getString("pass","");
        String  preference = shared.getString("preference","");
        String  Locpreference = shared.getString("Locpreference","");
        String Experience = shared.getString("Experience","");
        String Subject = shared.getString("Subject","");
        String Travel= shared.getString("Travel","");
        String Description = shared.getString("Description","");
        String url = shared.getString("url","");

        textview_name.setText(name);
        textview_email.setText(email);
        textview_contact.setText(contact);
        textview_pass.setText(pass);
        textview_preference.setText(preference);
        textview_locpref.setText(Locpreference);
        textview_exp.setText(Experience);
        textview_subject.setText(Subject);
        textview_travel.setText( Travel);
        textview_desc.setText(Description);
        Glide.with(this).load(url).into(profileimage);


    }

}
