package com.example.adarsh.nexteduv01;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class Insertphoto extends AppCompatActivity {
    CircleImageView imageView_UploadPic;
    Button selectbtn;
    TextView textView_finish;
    EditText editText_description;
    Uri fileUri;
    public static String UPLOAD_URL="https://adarshrao.000webhostapp.com/images/upload2.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertphoto);
        imageView_UploadPic=findViewById(R.id.imageView_UploadPic);
        selectbtn=findViewById(R.id.selectbtn);
        textView_finish=findViewById(R.id.textView_finish);
        editText_description=findViewById(R.id.editText_description);

        textView_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String description =editText_description.getText().toString();
                String path=getPath(fileUri);
                SharedPreferences sharedPreferences = getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("Description",description);
                edit.putString("path",path);
                edit.apply();
                Toast.makeText(Insertphoto.this, "data saved", Toast.LENGTH_SHORT).show();
                upload();

            }
        });

        selectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                permission();

            }
        });

    }

    private void permission() {

        if (ContextCompat.checkSelfPermission(Insertphoto.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Insertphoto.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        } else {
            Logic();
        }

    }

    private void Logic() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent,"Please Choose Image"),124);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Logic();
            } else {
                Toast.makeText(this, "Deny", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        fileUri=data.getData();

        try{
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),fileUri);
            imageView_UploadPic.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    public void upload()
    {
        //String name =eV_Caption.getText().toString();
        //String path=getPath(fileUri);
        SharedPreferences sharedPreferences = getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("username","");
        String  email= sharedPreferences.getString("email","");
        String contact= sharedPreferences.getString("contact","");
        String password= sharedPreferences.getString("password","");
        String preference= sharedPreferences.getString("preference","");
        String Locpreference= sharedPreferences.getString("Locpreference","");
        String Experience= sharedPreferences.getString("Experience","");
        String subject= sharedPreferences.getString("Subject","");
        String Travel= sharedPreferences.getString("Travel","");
        String Description= sharedPreferences.getString("Description","");
        String imgpath = sharedPreferences.getString("path","");

        try{
            String uploadid= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadid,UPLOAD_URL)
                    .addFileToUpload(imgpath,"image")
                    .addParameter("name",username)
                    .addParameter("email",email)
                    .addParameter("contact",contact)
                    .addParameter("password",password)
                    .addParameter("preference",preference)
                    .addParameter("Locpreference",Locpreference)
                    .addParameter("Experience", Experience)
                    .addParameter("Subject",subject)
                    .addParameter("Travel",Travel)
                    .addParameter("Description",Description)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

}

}
