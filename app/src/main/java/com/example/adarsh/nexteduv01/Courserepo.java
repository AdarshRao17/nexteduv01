
package com.example.adarsh.nexteduv01;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Courserepo {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("course_id")
    @Expose
    private String courseId;
    @SerializedName("course_name")
    @Expose
    private String courseName;
    @SerializedName("course_address")
    @Expose
    private String courseAddress;
    @SerializedName("course_city")
    @Expose
    private String courseCity;
    @SerializedName("course_detail")
    @Expose
    private String courseDetail;
    @SerializedName("course_price")
    @Expose
    private String coursePrice;
    @SerializedName("course_date")
    @Expose
    private String courseDate;
    @SerializedName("course_duration")
    @Expose
    private String courseDuration;
    @SerializedName("course_intake")
    @Expose
    private String courseIntake;
    @SerializedName("url")
    @Expose
    private String url;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseAddress() {
        return courseAddress;
    }

    public void setCourseAddress(String courseAddress) {
        this.courseAddress = courseAddress;
    }

    public String getCourseCity() {
        return courseCity;
    }

    public void setCourseCity(String courseCity) {
        this.courseCity = courseCity;
    }

    public String getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(String courseDetail) {
        this.courseDetail = courseDetail;
    }

    public String getCoursePrice() {
        return coursePrice;
    }

    public void setCoursePrice(String coursePrice) {
        this.coursePrice = coursePrice;
    }

    public String getCourseDate() {
        return courseDate;
    }

    public void setCourseDate(String courseDate) {
        this.courseDate = courseDate;
    }

    public String getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(String courseDuration) {
        this.courseDuration = courseDuration;
    }

    public String getCourseIntake() {
        return courseIntake;
    }

    public void setCourseIntake(String courseIntake) {
        this.courseIntake = courseIntake;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
