package com.example.adarsh.nexteduv01;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Coachpersonaldetails extends AppCompatActivity {
    EditText input;
    Button btn_about,btn_city,btn_college,btn_qualification,btn_achievement;
    TextView txt_about,txt_city,txt_college,txt_qualification,txt_achievement;

    AlertDialog alertdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coachpersonaldetails);
        btn_about= findViewById(R.id.btn_about);
        btn_city= findViewById(R.id.btn_city);
        btn_college= findViewById(R.id.btn_college);
        btn_qualification= findViewById(R.id.btn_qualification);
        btn_achievement= findViewById(R.id.btn_achievement);
        txt_about=findViewById(R.id.txt_about);
        txt_city=findViewById(R.id.txt_city);
        txt_college=findViewById(R.id.txt_college);
        txt_qualification=findViewById(R.id.txt_qualification);
        txt_achievement=findViewById(R.id.txt_achievement);

        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        final String id = shared.getString("id","");


        //to fetch data every second
        final Handler handler =new Handler();
        Timer timer =new Timer();
        TimerTask task =new TimerTask() {
            @Override
            public void run() {
                fetchpersonalinfo(id);
            }
        };
        timer.schedule(task,0,5000);

        //for about
       btn_about.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               alertdialog = new AlertDialog.Builder(Coachpersonaldetails.this).create();
               final EditText input = new EditText(Coachpersonaldetails.this);
               input.setText(txt_about.getText().toString());
               alertdialog.setTitle("About");
               alertdialog.setMessage("Tell us something about yourself.");
               alertdialog.setView(input);
               alertdialog.setCancelable(false);
               alertdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {

                       alertdialog.dismiss();

                   }
               });

               alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "SUBMIT", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {

                       Toast.makeText(getApplicationContext(), "Text entered is " + input.getText().toString(), Toast.LENGTH_SHORT).show();

                   }
               });
               alertdialog.show();

           }
       });

        //for college
        btn_college.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog = new AlertDialog.Builder(Coachpersonaldetails.this).create();
                final EditText input = new EditText(Coachpersonaldetails.this);
                input.setText(txt_college.getText().toString());
                alertdialog.setTitle("COLLEGE");
                alertdialog.setMessage("Enter your college & university name");
                alertdialog.setView(input);
                alertdialog.setCancelable(false);
                alertdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alertdialog.dismiss();

                    }
                });

                alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "SUBMIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Toast.makeText(getApplicationContext(), "Text entered is " + input.getText().toString(), Toast.LENGTH_SHORT).show();

                    }
                });
                alertdialog.show();

            }
        });

        //for qualification
        btn_qualification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog = new AlertDialog.Builder(Coachpersonaldetails.this).create();
                final EditText input = new EditText(Coachpersonaldetails.this);
                input.setText(txt_qualification.getText().toString());
                alertdialog.setTitle("QUALIFICATION");
                alertdialog.setMessage("Enter your qualification details");
                alertdialog.setView(input);
                alertdialog.setCancelable(false);
                alertdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alertdialog.dismiss();

                    }
                });

                alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "SUBMIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Toast.makeText(getApplicationContext(), "Text entered is " + input.getText().toString(), Toast.LENGTH_SHORT).show();

                    }
                });
                alertdialog.show();

            }
        });

        //for achievements
        btn_achievement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog = new AlertDialog.Builder(Coachpersonaldetails.this).create();
                final EditText input = new EditText(Coachpersonaldetails.this);
                input.setText(txt_achievement.getText().toString());
                alertdialog.setTitle("ACHIEVEMENTS");
                alertdialog.setMessage("Enter your achievements details");
                alertdialog.setView(input);
                alertdialog.setCancelable(false);
                alertdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alertdialog.dismiss();

                    }
                });

                alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "SUBMIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Toast.makeText(getApplicationContext(), "Text entered is " + input.getText().toString(), Toast.LENGTH_SHORT).show();

                    }
                });
                alertdialog.show();

            }
        });

            btn_city.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertdialog = new AlertDialog.Builder(Coachpersonaldetails.this).create();
                    final EditText input = new EditText(Coachpersonaldetails.this);
                    input.setText(txt_city.getText().toString());
                    alertdialog.setTitle("CITY");
                    alertdialog.setMessage("Enter your city");
                    alertdialog.setView(input);
                    alertdialog.setCancelable(false);
                    alertdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            alertdialog.dismiss();

                        }
                    });

                    alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "SUBMIT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Toast.makeText(getApplicationContext(), "Text entered is " + input.getText().toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
                    alertdialog.show();
                }
            });




    }


    private void fetchpersonalinfo(final String id) {

        String path="https://adarshrao.000webhostapp.com/images/personalinfo.php";
        StringRequest stringRequest=new StringRequest(Request.Method.POST, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Toast.makeText(Profilepage.this,response.toString(), Toast.LENGTH_SHORT).show();
                Log.v("DATA",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String about = jsonObject1.getString("about");
                    //Toast.makeText(Profilepage.this, "data "+about, Toast.LENGTH_SHORT).show();
                    String city = jsonObject1.getString("city");
                    String college = jsonObject1.getString("college");
                    String qualification = jsonObject1.getString("qualification");
                    String  achievement = jsonObject1.getString("achievement");
                    txt_about.setText(about);
                    txt_city.setText(city);
                    txt_college.setText(college);
                    txt_qualification.setText(qualification);
                    txt_achievement.setText(achievement);

                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Coachpersonaldetails.this, "opps!", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMap = new HashMap<String, String>();
                stringMap.put("userid",id);
                return stringMap;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


}












