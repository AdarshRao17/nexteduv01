package com.example.adarsh.nexteduv01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
public class Course_list extends AppCompatActivity {
    Button addBtn;
    private RecyclerView userlist;
    private  Programadapter myadapter;
    private List<Courserepo> mycourses;
    public  SharedPreferences shared;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);
        mycourses=new ArrayList<>();
        userlist=findViewById(R.id.userlist);
        addBtn=findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(getApplicationContext(),Addcourse.class);
                startActivity(intent);
            }
        });

        shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String user_id = shared.getString("id","");


        showcoures(user_id);
    }



    private void showcoures(final String uid) {

        String path="https://adarshrao.000webhostapp.com/images/displaycourse.php";
        StringRequest stringRequest=new StringRequest(Request.Method.POST, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                 Log.v("DATA",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for(int i=0;i < jsonArray.length();i++)
                    {
                        JSONObject course =jsonArray.getJSONObject(i);
                        Courserepo courserepo = new Courserepo();
                        courserepo.setCourseName(course.getString("course_name"));
                        courserepo.setCourseCity(course.getString("course_city"));
                        courserepo.setUrl(course.getString("url"));
                        courserepo.setCourseDate(course.getString("course_date"));
                        courserepo.setCoursePrice(course.getString("course_price"));
                        courserepo.setCourseDuration(course.getString("course_duration"));
                        courserepo.setCourseDetail(course.getString("course_detail"));
                        courserepo.setCourseIntake(course.getString("course_intake"));
                        courserepo.setCourseAddress(course.getString("course_address"));
                        mycourses.add(courserepo);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setuprecylerview(mycourses);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Course_list.this, "opps!", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMap = new HashMap<String, String>();
                stringMap.put("userid",uid);
                return stringMap;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void setuprecylerview(List<Courserepo> mycourses) {

        myadapter= new Programadapter(this,mycourses);
        userlist.setLayoutManager(new LinearLayoutManager(this));
        userlist.setAdapter(myadapter);
    }
}
