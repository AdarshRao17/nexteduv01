package com.example.adarsh.nexteduv01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.jar.Attributes;

public class Signup extends AppCompatActivity {
    EditText editText_username,editText_email,editText_contact,editText_pass;
    Button button_register;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        editText_username=findViewById(R.id.editText_username);
        editText_email=findViewById(R.id.editText_email);
        editText_contact=findViewById(R.id.editText_contact);
        editText_pass= findViewById(R.id.editText_pass);
        button_register=findViewById(R.id.button_register);

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //taking values from edit text
                final String user_name= editText_username.getText().toString();
                final String user_email= editText_email.getText().toString();
                final String user_contact= editText_contact.getText().toString();
                final String user_password= editText_pass.getText().toString();
                //some validation
                if (  user_name.isEmpty() || user_email.isEmpty() || user_contact.isEmpty() ||user_password.isEmpty()) {
                    Toast.makeText(Signup.this, "fill details", Toast.LENGTH_SHORT).show();

                } else {

                    //saving value
                    SharedPreferences sharedPreferences = getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("username",user_name);
                    edit.putString("email",user_email);
                    edit.putString("contact",user_contact);
                    edit.putString("password",user_password);
                    edit.apply();
                    Intent intent = new Intent(getApplicationContext(),form.class);
                    startActivity(intent);
                }






            }
        });
    }
}
