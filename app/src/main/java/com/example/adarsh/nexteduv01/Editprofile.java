package com.example.adarsh.nexteduv01;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class Editprofile extends AppCompatActivity {

    CircleImageView profileimage;
    TextView change_profile,editBtn;
    Uri fileUri;
    EditText editText_name,editText_email,editText_contact,editText_pass,editText_preference,
            editText_locpref,editText_exp,editText_subject,editText_desc,editText_travel;
    public static String UPLOAD_URL="https://adarshrao.000webhostapp.com/images/updateprofile.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        profileimage=findViewById(R.id.profileimage);
        change_profile=findViewById(R.id.change_profile);
        editBtn=findViewById(R.id.editBtn);
        editText_name=findViewById(R.id.editText_name);
        editText_email=findViewById(R.id.editText_email);
        editText_contact= findViewById(R.id.editText_contact);
        editText_pass=findViewById(R.id.editText_pass);
        editText_preference=findViewById(R.id.editText_preference);
        editText_locpref=findViewById(R.id. editText_locpref);
        editText_exp=findViewById(R.id.editText_exp);
        editText_subject=findViewById(R.id.editText_subject);
        editText_desc=findViewById(R.id.editText_desc);
        editText_travel=findViewById(R.id.editText_travel);

        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String name = shared.getString("name","");
        String email =shared.getString("email","");
        String contact = shared.getString("contact","");
        String pass = shared.getString("pass","");
        String  preference = shared.getString("preference","");
        String  Locpreference = shared.getString("Locpreference","");
        String Experience = shared.getString("Experience","");
        String Subject = shared.getString("Subject","");
        String Travel= shared.getString("Travel","");
        String Description = shared.getString("Description","");
        String url = shared.getString("url","");

        editText_name.setText(name);
        editText_email.setText(email);
        editText_contact.setText(contact);
        editText_pass.setText(pass);
        editText_preference.setText(preference);
        editText_locpref.setText(Locpreference);
        editText_exp.setText(Experience);
        editText_subject.setText(Subject);
        editText_travel.setText( Travel);
        editText_desc.setText(Description);
        Glide.with(this).load(url).into(profileimage);

        change_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                permission();

            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getting new data from the edittext.
                String path=getPath(fileUri);
                String name= editText_name.getText().toString();
                String email= editText_email.getText().toString();
                String contact= editText_contact.getText().toString();
                String password= editText_pass.getText().toString();
                String preference = editText_preference.getText().toString();
                String locpreference =  editText_locpref.getText().toString();
                String exp=editText_exp.getText().toString();
                String sub=editText_subject.getText().toString();
                String travel=editText_travel.getText().toString();
                String description =editText_desc.getText().toString();
                //takung id from mypref file
                SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                String id =shared.getString("id","");
                //saving every thing in a new file named 'Editedinfo'.
                SharedPreferences sharedPreferences = getSharedPreferences("Editedinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("name",name);
                edit.putString("email",email);
                edit.putString("contact",contact);
                edit.putString("password",password);
                edit.putString(" preference", preference);
                edit.putString("locpreference",locpreference);
                edit.putString("exp",exp);
                edit.putString("sub",sub);
                edit.putString("travel",travel);
                edit.putString("Description",description);
                edit.putString("path",path);
                edit.putString("id",id);
                edit.apply();
                Toast.makeText(Editprofile.this, "Edited data saved in file", Toast.LENGTH_SHORT).show();
                //uploading the edidted data
                upload();


            }
        });

    }

    private void upload() {

        SharedPreferences sharedPreferences = getSharedPreferences("Editedinfo", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("name","");
        String  email= sharedPreferences.getString("email","");
        String contact= sharedPreferences.getString("contact","");
        String password= sharedPreferences.getString("password","");
        String preference= sharedPreferences.getString(" preference","");
        String Locpreference= sharedPreferences.getString("locpreference","");
        String Experience= sharedPreferences.getString("exp","");
        String subject= sharedPreferences.getString("sub","");
        String Travel= sharedPreferences.getString("travel","");
        String Description= sharedPreferences.getString("Description","");
        String imgpath = sharedPreferences.getString("path","");
        String id =sharedPreferences.getString("id","");

        try {
            String uploadid = UUID.randomUUID().toString();
            new MultipartUploadRequest(this, uploadid, UPLOAD_URL)
                    .addFileToUpload(imgpath, "image")
                    .addArrayParameter("id",id)
                    .addParameter("name", name)
                    .addParameter("email", email)
                    .addParameter("contact", contact)
                    .addParameter("password", password)
                    .addParameter("preference", preference)
                    .addParameter("Locpreference", Locpreference)
                    .addParameter("Experience", Experience)
                    .addParameter("Subject", subject)
                    .addParameter("Travel", Travel)
                    .addParameter("Description", Description)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void permission() {
        if (ContextCompat.checkSelfPermission(Editprofile.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Editprofile.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        } else {
            Logic();
        }
    }

    private void Logic() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent,"Please Choose Image"),124);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Logic();
            } else {
                Toast.makeText(this, "Deny", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        fileUri=data.getData();

        try{
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),fileUri);
            profileimage.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
}
