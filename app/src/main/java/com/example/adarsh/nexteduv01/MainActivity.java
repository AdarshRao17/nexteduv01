package com.example.adarsh.nexteduv01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    TextView registerpage;
    EditText editText_lemail,editText_lpass;
    Button button_login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerpage= findViewById(R.id.registerpage);
        editText_lemail=findViewById(R.id.editText_lemail);
        editText_lpass=findViewById(R.id.editText_lpass);
        button_login=findViewById(R.id.button_login);

        registerpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Signup.class);
                startActivity(intent);
            }
        });

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //taking values from edit text
                 String user_email= editText_lemail.getText().toString();
                 String user_password= editText_lpass.getText().toString();

                //some validation
                if (user_email.isEmpty() || user_password.isEmpty()) {
                    Toast.makeText(MainActivity.this, "fill details", Toast.LENGTH_SHORT).show();

                } else {

                    login(user_email,user_password);
                }

            }
        });

    }

    public void login(final String email, final String pass){

        String path="https://adarshrao.000webhostapp.com/images/login.php";
        StringRequest stringRequest=new StringRequest(Request.Method.POST, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Toast.makeText(MainActivity.this,response.toString(), Toast.LENGTH_SHORT).show();
                Log.v("DATA",response);
                try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                        String id = jsonObject1.getString("id");
                        //Toast.makeText(MainActivity.this, "the id is "+id, Toast.LENGTH_SHORT).show();
                         String name = jsonObject1.getString("name");
                         String email = jsonObject1.getString("email");
                         String contact = jsonObject1.getString("contact");
                         String pass = jsonObject1.getString("password");
                         String  preference = jsonObject1.getString("preference");
                         String  Locpreference = jsonObject1.getString("Locpreference");
                         String Experience = jsonObject1.getString("Experience");
                         String Subject = jsonObject1.getString("Subject");
                         String Travel= jsonObject1.getString("Travel");
                         String Description = jsonObject1.getString("Description");
                         String url = jsonObject1.getString("url");
                         SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
                         SharedPreferences.Editor editor = shared.edit();
                         editor.putString("id",id);
                        editor.putString("name",name);
                        editor.putString("email",email);
                        editor.putString("contact",contact);
                        editor.putString("pass",pass);
                        editor.putString("preference",preference );
                        editor.putString("Locpreference",Locpreference);
                        editor.putString("Experience",Experience);
                        editor.putString("Subject",Subject );
                        editor.putString("Travel", Travel);
                        editor.putString("Description", Description);
                        editor.putString("url",url);
                        editor.apply();
                        Intent login = new Intent(getApplicationContext(),Profilepage.class);
                        startActivity(login);
                        //to prevent back click from user
                         MainActivity.this.finish();
                        //now if the user logs in successfully we must store the result
                        SharedPreferences setting = getSharedPreferences("Checkstatus", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = setting.edit();
                        editor1.putBoolean("hasLoggedIn",true);
                        editor1.apply();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(MainActivity.this, "opps!", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMap = new HashMap<String, String>();
                stringMap.put("uemail",email);
                stringMap.put("upassword",pass);
                return stringMap;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
}
