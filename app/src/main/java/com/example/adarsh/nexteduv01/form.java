package com.example.adarsh.nexteduv01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class form extends AppCompatActivity {
     EditText editText_exp,editText_sub,editText_travel;
     Spinner Preferencespinner,locationspinner;
     TextView textView_next1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        editText_exp=findViewById(R.id.editText_exp);
        editText_sub=findViewById(R.id.editText_sub);
        editText_travel=findViewById(R.id.editText_travel);
        Preferencespinner=findViewById(R.id.Preferencespinner);
        locationspinner=findViewById(R.id.locationspinner);
        textView_next1=findViewById(R.id.textView_next1);

        final String [] data1={"One to one tution","Group tution","crash course","Regular"};
        ArrayAdapter<String> arrayAdapter1= new ArrayAdapter(getApplicationContext(),R.layout.item_list1,R.id.textView, data1);
        Preferencespinner.setAdapter(arrayAdapter1);


        final String [] data2={"Tutor's home","Students home"};
        ArrayAdapter<String> arrayAdapter2= new ArrayAdapter(getApplicationContext(),R.layout.item_list1,R.id.textView, data2);
        locationspinner.setAdapter(arrayAdapter2);

        textView_next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String exp=editText_exp.getText().toString();
                String sub=editText_sub.getText().toString();
                String travel=editText_travel.getText().toString();
                String preference = Preferencespinner.getSelectedItem().toString();
                String locpreference = locationspinner.getSelectedItem().toString();
                //some validation
                if (exp.isEmpty() || sub.isEmpty() || travel.isEmpty() || preference.isEmpty() ||  locpreference.isEmpty()) {
                    Toast.makeText(form.this, "fill details", Toast.LENGTH_SHORT).show();

                } else {

                    SharedPreferences sharedPreferences = getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("Experience",exp);
                    edit.putString("Subject",sub);
                    edit.putString("Travel",travel);
                    edit.putString("preference",preference);
                    edit.putString("Locpreference",locpreference);
                    edit.apply();
                    Toast.makeText(form.this, "data saved", Toast.LENGTH_SHORT).show();
                    Intent intent =new Intent(getApplicationContext(),Insertphoto.class);
                    startActivity(intent);

                }


            }
        });

    }
}
