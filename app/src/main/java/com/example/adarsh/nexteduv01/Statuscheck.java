package com.example.adarsh.nexteduv01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Statuscheck extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statuscheck);

        SharedPreferences setting = getSharedPreferences("Checkstatus", Context.MODE_PRIVATE);
        //getting the value from hasLoggedIn value if there is no value present then the default value would be false
        boolean hasLoggedIn = setting.getBoolean("hasLoggedIn",false);
        if(hasLoggedIn)
        {
            Intent intent =new Intent(getApplicationContext(),Profilepage.class);
            startActivity(intent);
            Statuscheck.this.finish();
        }
        else
        {
            Intent intent= new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            Statuscheck.this.finish();

        }
    }
}
