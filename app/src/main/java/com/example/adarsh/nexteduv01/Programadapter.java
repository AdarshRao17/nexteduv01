package com.example.adarsh.nexteduv01;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

public class Programadapter extends RecyclerView.Adapter<Programadapter.programviewholder> {
    private Context context;
    private Courserepo[] data;
    public Programadapter(Context context, List<Courserepo> data) {
        this.context=context;
        this.data= data.toArray(new Courserepo[0]);

    }

    @NonNull
    @Override
    public programviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater l = LayoutInflater.from(parent.getContext());
        View v = l.inflate(R.layout.course_list,parent,false);
        //now new view holder is created
        return new programviewholder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final programviewholder holder, int position) {

        final Courserepo user=data[position];
        holder.textuser.setText(user.getCourseName());
        holder.date.setText(user.getCourseDate());
        holder.price.setText(user.getCoursePrice());
        holder.duration.setText(user.getCourseDuration());
        holder.description.setText(user.getCourseDetail());
        //to bind the image view we will use glide library
        Glide.with(holder.imguser.getContext()).load(user.getUrl()).into(holder.imguser);
        //setting up an onclick listener
        holder.parentlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set details first in course_list.java then get details.
                Intent intent = new Intent(context,Onclickadapter.class);
                intent.putExtra("course_name",user.getCourseName());
                intent.putExtra("course_city",user.getCourseCity());
                intent.putExtra("course_date",user.getCourseDate());
                intent.putExtra("course_image",user.getUrl());
                intent.putExtra("course_intake",user.getCourseIntake());
                intent.putExtra("course_detail",user.getCourseDetail());
                intent.putExtra("course_address",user.getCourseAddress());
                intent.putExtra("course_duration",user.getCourseDuration());
                intent.putExtra("course_price",user.getCoursePrice());


                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public  class programviewholder extends RecyclerView.ViewHolder
    {
        ImageView imguser;
        TextView textuser,date,price,duration,description;
        LinearLayout parentlayout;
        public programviewholder(View itemView) {
            super(itemView);
            parentlayout=itemView.findViewById(R.id.parentlayout);
            imguser=itemView.findViewById(R.id.imguser);
            textuser=itemView.findViewById(R.id.textuser);
            date=itemView.findViewById(R.id.date);
            price=itemView.findViewById(R.id.price);
            duration=itemView.findViewById(R.id.duration);
            description=itemView.findViewById(R.id.description);
        }
    }
}
