package com.example.adarsh.nexteduv01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddReview extends AppCompatActivity {

EditText EV_name,EV_review;
RatingBar RB_rate;
Button BTN_add;
private String URL_REVIEW="https://adarshrao.000webhostapp.com/images/addreview.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review);

        EV_name=findViewById(R.id.EV_name);
        EV_review=findViewById(R.id.EV_review);
        RB_rate=findViewById(R.id.RB_rate);
        BTN_add=findViewById(R.id.BTN_add);

        BTN_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendreview();
            }
        });

    }

            private void sendreview() {

                final String name=EV_name.getText().toString();
                final Float rate=RB_rate.getRating();
                final String review=EV_review.getText().toString();

                StringRequest request=new StringRequest(Request.Method.POST, URL_REVIEW, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if(response.contains("DataInsertedSuccessfully"))
                        {
                            Toast.makeText(getApplicationContext(), "Data Inserted", Toast.LENGTH_SHORT).show();

                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

                    }
                })
                {
                    @Override
                    protected Map<String,String> getParams() throws AuthFailureError {
                        Map<String,String> map=new HashMap<>();
                        map.put("uname",name);
                        map.put("urating",rate.toString());
                        map.put("ureview",review);

                        return map;


                    }
                };
                RequestQueue queue= Volley.newRequestQueue(this);
                request.setShouldCache(false);
                queue.add(request);



            }

}
