package com.example.adarsh.nexteduv01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Onclickadapter extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coursedetaillayout);
        //always add activity into manifest to avoid app crash,now to initiate this process we need to make changes in programadapter
        getIncomingIntent();
    }

    private void getIncomingIntent()
    {
        //check if the intent has any extras before getting it ,else the app would crash.
        if(getIntent().hasExtra("course_name") && getIntent().hasExtra("course_city") && getIntent().hasExtra("course_date")
        && getIntent().hasExtra("course_image")  && getIntent().hasExtra("course_intake")  && getIntent().hasExtra("course_detail")
                && getIntent().hasExtra("course_address")  && getIntent().hasExtra("course_duration")  && getIntent().hasExtra("course_price"))
        {
            String cname = getIntent().getStringExtra("course_name");
            String ccity = getIntent().getStringExtra("course_city");
            String cdate = getIntent().getStringExtra("course_date");
            String cimage = getIntent().getStringExtra("course_image");
            String cintake = getIntent().getStringExtra("course_intake");
            String cdetail = getIntent().getStringExtra("course_detail");
            String caddress = getIntent().getStringExtra("course_address");
            String cduration = getIntent().getStringExtra("course_duration");
            String cprice = getIntent().getStringExtra("course_price");

            setContent(cname,ccity,cdate,cimage,cintake,cdetail,caddress,cduration,cprice);
        }
    }

    private void setContent(String cname,String ccity,String cdate,String cimage,String cintake,String cdetail,String caddress,String cduration,String cprice)
    {
        TextView cdName,cdCity,cdDate,cdIntake,cdDetail,cdAddress,cdDuration,cdPrice;
        ImageView cdImage;
        Button back;
        cdName=findViewById(R.id.cdName);
        cdCity=findViewById(R.id.cdCity);
        cdDate=findViewById(R.id.cdDate);
        cdImage=findViewById(R.id.cdImage);
        cdIntake=findViewById(R.id.cdIntake);
        cdDetail=findViewById(R.id.cdDetail);
        cdAddress=findViewById(R.id.cdAddress);
        cdDuration=findViewById(R.id.cdDuration);
        cdPrice=findViewById(R.id.cdPrice);
        back=findViewById(R.id.back);
        cdName.setText(cname);
        cdCity.setText(ccity);
        cdDate.setText(cdate);
        cdIntake.setText(cintake);
        cdDetail.setText(cdetail);
        cdAddress.setText(caddress);
        cdDuration.setText(cduration);
        cdPrice.setText(cprice);
        Glide.with(this).load(cimage).into(cdImage);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Course_list.class);
                startActivity(intent);
                Onclickadapter.this.finish();
            }
        });
    }
}
