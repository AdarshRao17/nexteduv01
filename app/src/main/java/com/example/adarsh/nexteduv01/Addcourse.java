package com.example.adarsh.nexteduv01;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class Addcourse extends AppCompatActivity {

    CircleImageView profileimage;
    TextView coach_name;
    EditText course_Date,course_Name,Course_Address,Course_City,Course_Details,course_Price,course_Duration,course_Intake;
    int mdate,mmonth,myear;
    ImageView course_image;
    Button submitBtn;
    Uri fileUri;
    public static String UPLOAD_URL="https://adarshrao.000webhostapp.com/images/addcourse.php";
    String cname,caddress,ccity,cdetail,cprice,cdate,cduration, cintake, path;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcourse);
        profileimage=findViewById(R.id.profileimage);
        coach_name=findViewById(R.id.coach_name);
        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String name = shared.getString("name","");
        String url = shared.getString("url","");
        coach_name.setText(name);
        course_Date=findViewById(R.id.course_Date);
        course_image=findViewById(R.id.course_image);
        Course_Address=findViewById(R.id.Course_Address);
        Course_City=findViewById(R.id.Course_City);
        Course_Details=findViewById(R.id.Course_Details);
        course_Price=findViewById(R.id.course_Price);
        course_Duration=findViewById(R.id.course_Duration);
        course_Intake=findViewById(R.id.course_Intake);
        course_Name=findViewById(R.id.course_Name);
        submitBtn=findViewById(R.id.submitBtn);
        Glide.with(this).load(url).into(profileimage);
        dialog= new ProgressDialog(this);
        dialog.setTitle("Adding new course");
        dialog.setMessage("please Wait this may take few seconds");

        Calendar calendar = Calendar.getInstance();
        mdate=calendar.get(Calendar.DAY_OF_MONTH);
        mmonth=calendar.get(Calendar.MONTH);
        myear=calendar.get(calendar.YEAR);

        course_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DatePickerDialog datePickerDialog= new DatePickerDialog(Addcourse.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        course_Date.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                    }
                },mdate,mmonth,myear);

                datePickerDialog.show();
            }
        });

        course_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permission();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
               cname = course_Name.getText().toString();
               caddress = Course_Address.getText().toString();

               ccity =   Course_City.getText().toString();

               cdetail =  Course_Details.getText().toString();

               cprice = course_Price.getText().toString();

               cdate = course_Date.getText().toString();

               cduration = course_Duration.getText().toString();

               cintake =course_Intake.getText().toString();

               path=getPath(fileUri);


                if (cname.isEmpty()||caddress.isEmpty()||ccity.isEmpty()||cdetail.isEmpty()||cprice.isEmpty()||cdate.isEmpty()||cduration.isEmpty()||cintake.isEmpty()||path.isEmpty())
                {

                Toast.makeText(Addcourse.this, "Enter All Details", Toast.LENGTH_SHORT).show();

                }   else {

                   Log.e("Success", "Verified");
                    upload();

                }


            }
        });

    }

    private void upload() {

        SharedPreferences shared = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        String user_id = shared.getString("id","");
        try {
            String uploadid = UUID.randomUUID().toString();
            new MultipartUploadRequest(Addcourse.this, uploadid, UPLOAD_URL)
                    .addFileToUpload(path, "image")
                    .addParameter("user_id",user_id)
                    .addParameter("cname", cname)
                    .addParameter("caddress", caddress)
                    .addParameter("ccity", ccity)
                    .addParameter("cdetail", cdetail)
                    .addParameter("cprice", cprice)
                    .addParameter("cdate",cdate)
                    .addParameter("cduration", cduration)
                    .addParameter("cintake",cintake)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
            Intent intent=new Intent(getApplicationContext(),Profilepage.class);
            startActivity(intent);

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Addcourse.this.finish();
    }


    private void permission() {
        if (ContextCompat.checkSelfPermission(Addcourse.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Addcourse.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        } else {
            Logic();
        }
    }

    private void Logic() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent,"Please Choose Image"),124);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Logic();
            } else {
                Toast.makeText(this, "Deny", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        fileUri=data.getData();

        try{
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),fileUri);
            course_image.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


}

